<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class DashboardController
 * @package App\Controller
 *
 */
class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     * @Route("/", name="index2")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('index.html.twig');
    }

}